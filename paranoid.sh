#!/bin/bash

# Save some info before "closing the doors"
record_activity() 
{
    echo "Saving net connections..."
    netstat -aple >netstat_$(date +%Y-%m-%d_%T).log
    echo "Saving active users..."
    w >active_users_$(date +%Y-%m-%d_%T).log
    echo "Saving last login..."
    last >last_login_$(date +%Y-%m-%d_%T).log
    echo "Saving process list..."
    ps aux >process_$(date +%Y-%m-%d_%T).log
    echo "Saving mount points..."
    mountpoints_file=mount_points_$(date +%Y-%m-%d_%T).log
    mount >$mountpoints_file
    df -h >>$mountpoints_file
    echo "Saving user accounts..."
    cat /etc/passwd >users_$(date +%Y-%m-%d_%T).log
}

kill_processes()
{
    # processes to kill
    to_kill=("p1" "p2" "p3")
    
    # All processes
    processes=$(ps -aux | awk '{print $2":"$11}')
    
    for p in $processes
    do
        pid=$(echo $p | cut -d':' -f1 -)
        pname=$(echo $p | cut -d ':' -f2 -)
        echo "Process: $pname Pid: $pid"
        # Kill process if process (value) is in the array (to_kill)
        if [[ "${to_kill[@]}" =~ "${pname}" ]]
        then
            echo "-> killing process $pid"
            #kill -9 $pid
        fi
    done
}

stop_services()
{
    # List of services you want to stop
    services=("apache2" "apparmor" "avahi-daemon" "bluetooth" 
    "mysql" "ntop" "thermald" "ufw" 
    "urandom" "virtualbox-guest-utils" "lm-sensors" 
    "apache-htcacheclean" "cups-browsed" "hddtemp" 
    "openvpn" "ntp" "ondemand" "php7.0-fpm")

    for s in "${services[@]}"
    do
        echo "Stopping service $s"
    #    service $s stop
    done
}

kill_netcon_processes()
{
    # Get process ids from tcp/udp connections
    # by reading column 9 and spliting each
    # value separated with '/' being f1 the
    # process id and f2 the process name
    tcp_udp=$(netstat -aple | 
            egrep -i "tcp|udp" | 
            awk '{print $9}' | 
            cut -d'/' -f1 -)

    # Kill valid (not equal to '-') pids
    for pid in $tcp_udp
    do
        if [ "$pid" != "-" ]
        then
            echo "-> Killing net process: $pid"
    #       kill -9 $pid
        fi
    done
}

# Check if more than one parameter is passed
if [ $# -gt 1 ]
then
    echo "Too many parameters"
    echo "usage: (as root / sudo)"
    echo "  $0 services|processes|netcon"
    echo "or just:"
    echo "  $0"
    exit 1
fi

# Check if "services" parameter is passed
if [ $# -eq 1 ] && [ $1 = "services" ]
then
    echo "Stopping services only..."
    stop_services
    exit 1
fi

# Check if "processes" parameter is passed
if [ $# -eq 1 ] && [ $1 = "processes" ]
then
    echo "Stopping processes only..."
    kill_processes
    exit 1
fi

# Check if "netcon" parameter is passed
if [ $# -eq 1 ] && [ $1 = "netcon" ]
then
    echo "Stopping processes for net connections only..."
    kill_netcon_processes
    exit 1
fi

# Function call
record_activity

# Save list of opened files
if [ -x "$(command -v lsof)" ]
then
    lsof >opened_files_$(date +%Y-%m-%d_%T).log
fi

# Default behaviour (no parameters)
lsmod >loaded_modules.log
kill_processes
stop_services
kill_netcon_processes

# Memory dump (needs fmem module compilation
#dd if=/dev/fmem of=/root/mem_(date +%Y-%m-%d_%T).log

if [ -x "$(command -v iptables)" ]
then
    echo "iptables is available"
    echo "Deleting rules..."
#    iptables -F
#    iptables -X
    echo "Blocking input traffic"
#    iptables -P INPUT DROP
    echo "Blocking output traffic"
#    iptables -P OUTPUT DROP
    echo "Blocking forwarded traffic"
#    iptables -P FORWARD DROP
fi

if [ -f "/proc/net/dev" ]
then
    # Get list of interfaces
    interfaces=$(cat /proc/net/dev | tail -f -n 2 | cut -d':' -f1 -)
    for i in $interfaces
    do
        # Disable all except 'lo' interface
        if [ "$i" != "lo" ]
        then   
            echo "Disabling interface: ifconfig $i down"       
            #ifconfig $i down
        fi
    done
fi

# TODO: Send recorded data to other machine/s
if [ -x "$(command -v python)" ]
then
    python -c "import socket;"
fi
